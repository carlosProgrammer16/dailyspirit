import firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyBb19oJvfGXkYZXHQ2VdR27tJBcdA3CWtY",
    authDomain: "dailyspiritjuice.firebaseapp.com",
    databaseURL: "https://dailyspiritjuice.firebaseio.com",
    projectId: "dailyspiritjuice",
    storageBucket: "dailyspiritjuice.appspot.com",
    messagingSenderId: "853999006270",
    appId: "1:853999006270:web:3d438a641fd8eeb6"
  };
  // Initialize Firebase
  const fbase = firebase.initializeApp(firebaseConfig);

  export{fbase}
